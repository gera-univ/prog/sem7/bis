extern ExitProcess
extern show_register_dump

global WinMain

section .text
WinMain:
	sub	rsp, 8
	sub	rsp, 32

	mov	rax, 0AAAAAAAAh
	mov	rbx, 0BBBBBBBBh
	mov	rcx, 0CCCCCCCCh
	mov	rdx, 0DDDDDDDDh

	call	show_register_dump 

	mov	ecx, 0
	call	ExitProcess
