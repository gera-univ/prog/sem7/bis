extern MessageBoxA

global show_register_dump

section .data
digits:   db     "0123456789ABCDEF"
text_rax: db     "RAX",9,"........",10,13
text_rbx: db     "RBX",9,"........",10,13
text_rcx: db     "RCX",9,"........",10,13
text_rdx: db     "RDX",9,"........",10,13
text_rsp: db     "RSP",9,"........",10,13
text_rbp: db     "RBP",9,"........",10,13
text_rsi: db     "RSI",9,"........",10,13
text_rdi: db     "RDI",9,"........",10,13
text_r8:  db      "R8",9,"........",10,13
text_r9:  db      "R9",9,"........",10,13
text_r10: db     "R10",9,"........",10,13
text_r11: db     "R11",9,"........",10,13
text_r12: db     "R12",9,"........",10,13
text_r13: db     "R13",9,"........",10,13
text_r14: db     "R14",9,"........",10,13
text_r15: db     "R15",9,"........",10,13,0
text_caption: db "registers dump", 0

section .text
show_register_dump:
	push	rbp
	mov	rbp, rsp

	; save old register values
	push	rax
	push	rbx
	push	rcx
	push	rdx
	push	r8
	push	r9
	push	r10
	push	r11
	push	r12
	push	r13
	push	r14
	push	r15

	; save r8 once more since its used to store address
	push	r8

	push	rax
	lea	r8, [rel text_rax + 3]
	call	print_hex_qword
	pop	rax

	xchg	rax, rbx
	lea	r8, [rel text_rbx + 3]
	call	print_hex_qword
	xchg	rax, rbx

	xchg	rax, rcx
	lea	r8, [rel text_rcx + 3]
	call	print_hex_qword
	xchg	rax, rcx

	xchg	rax, rdx
	lea	r8, [rel text_rdx + 3]
	call	print_hex_qword
	xchg	rax, rdx

	push	rax
	mov	rax, rsp
	lea	r8, [rel text_rsp + 3]
	call	print_hex_qword
	pop	rax

	push	rax
	mov	rax, rbp
	lea	r8, [rel text_rbp + 3]
	call	print_hex_qword
	pop	rax

	push	rax
	mov	rax, rsi
	lea	r8, [rel text_rsi + 3]
	call	print_hex_qword
	pop	rax

	push	rax
	mov	rax, rdi
	lea	r8, [rel text_rdi + 3]
	call	print_hex_qword
	pop	rax

	pop	r8
	xchg	rax, r8
	lea	r8, [rel text_r8 + 2]
	call	print_hex_qword
	xchg	rax, r8
	
	xchg	rax, r9
	lea	r8, [rel text_r9 + 2]
	call	print_hex_qword
	xchg	rax, r9

	xchg	rax, r10
	lea	r8, [rel text_r10 + 3]
	call	print_hex_qword
	xchg	rax, r10

	xchg	rax, r11
	lea	r8, [rel text_r11 + 3]
	call	print_hex_qword
	xchg	rax, r11

	xchg	rax, r12
	lea	r8, [rel text_r12 + 3]
	call	print_hex_qword
	xchg	rax, r12

	xchg	rax, r13
	lea	r8, [rel text_r13 + 3]
	call	print_hex_qword
	xchg	rax, r13

	xchg	rax, r14
	lea	r8, [rel text_r14 + 3]
	call	print_hex_qword
	xchg	rax, r14

	xchg	rax, r15
	lea	r8, [rel text_r15 + 3]
	call	print_hex_qword
	xchg	rax, r15

	mov	rcx, 0
	lea	rdx, [rel text_rax]
	lea	r8, [rel text_caption]
	mov	r9d, 0
	call	MessageBoxA

	; restore old register values
	pop	r15
	pop	r14
	pop	r13
	pop	r12
	pop	r11
	pop	r10
	pop	r9
	pop	r8
	pop	rdx
	pop	rcx
	pop	rbx
	pop	rax

	jmp	return

print_hex_qword: 
; RAX	value to print
; R8	where to print
	push	rbp
	mov	rbp, rsp

	; save old register values
	push	rbx
	push	rcx
	push	rdx

	mov	rcx, 8
loop:
	mov	rbx, rax
	and	rbx, 0xF

	mov	rdx, digits
	add	rdx, rbx
	mov	rdx, [rdx]

	mov	rbx, r8
	add	rbx, rcx
	mov	byte [rel rbx], dl

	shr	rax, 4
	loop	loop

	; restore old register values
	pop	rdx
	pop	rcx
	pop	rbx

	jmp	return

return:
	mov   rsp, rbp
	pop   rbp
	ret
