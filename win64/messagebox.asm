extern MessageBoxA
extern FindWindowA
extern ExitThread 
extern ExitProcess
extern GetWindowRect
extern MoveWindow
extern CreateThread 
extern Sleep 

global WinMain

section .data
text_message: db "Hello, World!", 0
text_caption: db "64-bit hello!", 0

section .bss
alignb	8
handle: resq 1
rect: resd 4
deltax: resd 1
deltay: resd 1

section .text
WinMain:
	sub	rsp, 8
	sub	rsp, 32

	mov	rcx, 0
	mov	rdx, 0
	mov	r8, query_messagebox
	mov	r9, 0
	mov	qword [rsp + 4 * 8], 0
	mov	qword [rsp + 5 * 8], 0
	call	CreateThread

	mov	rcx, 0
	lea	rdx, [rel text_message]
	lea	r8, [rel text_caption]
	mov	r9d, 0
	call	MessageBoxA

	mov	ecx, 0
	call	ExitProcess

query_messagebox:
	mov	rcx, 1000
	call	Sleep

	mov	rcx, 0
	lea	rdx, [rel text_caption]
	call	FindWindowA

	mov	qword [rel handle], rax

loop:
	mov	rcx, qword [rel handle]
	mov	rdx, rect
	call	GetWindowRect

	rdtscp
	mov	edx, 0
	mov	ecx, 11
	div	ecx
	sub	edx, 5
	mov	dword [rel deltax], edx
	rdtscp
	mov	edx, 0
	mov	ecx, 11
	div	ecx
	sub	edx, 5
	mov	dword [rel deltay], edx

	mov	rcx, qword [rel handle]
	mov	r9d, dword [rel rect + 4 * 2] ; width
	sub	r9d, dword [rel rect]         ; (right - left)
	mov	eax, dword [rel rect + 4 * 3] ; height
	sub	eax, dword [rel rect + 4]     ; (bottom - top)

	mov	edx, dword [rel rect]         ; x (left corner)
	add	edx, dword [rel deltax]
	mov	r8d, dword [rel rect + 4]     ; y (top corner)
	add	r8d, dword [rel deltay]
	mov	qword [rsp + 4 * 8], rax
	mov	qword [rsp + 5 * 8], 0
	call	MoveWindow

	mov	rcx, 15
	call	Sleep

	jmp	loop
