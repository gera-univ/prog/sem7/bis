#include <stdio.h>
#include <windows.h>

char text[] = "Hello, World!";
char caption[] = "Hello";

DWORD query_messagebox(LPVOID param)
{
	srand(0);
	Sleep(1000);
	HWND handle = FindWindowA(0, caption);
	printf("%d\n", handle);
	RECT dimensions;
	GetWindowRect(handle, &dimensions);
	LONG width = dimensions.right-dimensions.left;
	LONG height = dimensions.bottom-dimensions.top;

	LONG x = dimensions.left;
	LONG y = dimensions.top;
	for (;;) {
		MoveWindow(handle, x, y, width, height, FALSE);
		x += rand() % 5 - 2;
		y += rand() % 5 - 2;
		Sleep(10);
	}
}

int main()
{
	CreateThread(0, 0, query_messagebox, 0, 0, 0);
	MessageBoxA(0, text, caption, 0);
	return 0;
}
