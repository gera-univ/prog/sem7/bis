Решения заданий по предмету *Безопасность Информационных Систем*

# DOS

Используйте программу [HelpPC](https://archive.org/details/msdos_shareware_fb_HELPPC) для справки. Есть [веб-версия](http://helppc.netcore2k.net/topics).

## [DOSBox-X](https://dosbox-x.com/)

Путь к конфигу: `dosbox-x -printconf`

```
[sdl]
output=openglnb

[render]
aspect_ratio=18:10
aspect=true

[autoexec]
mount C ~/prog/dos
C:
keyb ru 866 # Изменение языка ввода: Alt + Right/Left Shift
```

[Настройки для производительности](https://dosbox-x.com/wiki/Home#_frequently_asked_questions_faq)

[Как использовать отладчик](https://github.com/joncampbell123/dosbox-x/blob/master/README.debugger)

## MASM

[Архив с `MASM.EXE`, `LINK.EXE`, `TD.EXE`](http://www.mediafire.com/file/mm7cjztce9efj4w/8086.zip) 

# NASM

[Writing 16-bit Code](https://www.nasm.us/xdoc/2.15.05/html/nasmdoc9.html)

## Windows

[Примеры 32- и 64-битных программ на NASM под Windows](https://www.davidgrantham.com/)

## Linux

[NASM Tutorial](https://cs.lmu.edu/~ray/notes/nasmtutorial/)

[asmtutor](https://asmtutor.com/)

# C

[GCC-Inline-Assembly-HOWTO](https://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html)

[How to build DOS COM files with GCC](https://nullprogram.com/blog/2014/12/09/)

# Квайны

[x86 Quine: These 12 Bytes Print Themselves](https://susam.net/blog/x86-quine.html)

[Quine by Hannu](https://demozoo.org/productions/305270/)
