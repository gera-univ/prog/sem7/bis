asm (".code16gcc\n"
	"call	dosmain\n"
	"mov	$0x4C, %ah\n"
	"int	$0x21\n");

static void move_cursor(unsigned char row, unsigned char col)
{
	unsigned short pos = (row << 8) + col;
	asm volatile (
		"mov	$0x02, %%ah\n"
		"int	$0x10\n"
		:
		: "d"(pos));
}

static void move_cursor_right()
{
	asm volatile (
		"mov	$0x03, %ah\n"
		"int	$0x10\n"
		"add	$1, %dl\n"
		"mov	$0x02, %ah\n"
		"int	$0x10\n");
}

static void putchar(unsigned char chr)
{
	asm volatile (
		"mov	$0xA, %%ah\n"
		"mov	$1, %%cx\n"
		"int	$0x10\n"
		:
		: "a"(chr));
	move_cursor_right();
}

static void put4bit(unsigned char number)
{
	number &= 0xF;
	if (number < 10)
		putchar('0' + number);
	else
		putchar('A' + number - 10);
}

static void put8bit(unsigned char number)
{
	put4bit(number >> 4);
	put4bit(number);
}

int dosmain(void)
{
	for (unsigned char i = 0; i < 16; ++i) {
		for (unsigned char j = 0; j < 16; ++j) {
			move_cursor(j, i*5);
			unsigned char symcode = j + i*16;
			putchar(symcode);
			putchar('_');
			put8bit(symcode);
			putchar(' ');
		}
	}
	return 0;
}
