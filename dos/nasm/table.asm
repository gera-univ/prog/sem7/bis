segment data
digits: db "0123456789ABCDEF"

segment code 
..start:
        mov     ax,data 
        mov     ds,ax 
        mov     ax,stack 
        mov     ss,ax 
        mov     sp,stacktop

	mov	cx, 255
loop:
	push cx

	; get the opposite of counter into al
	mov	al, cl
	mov	ah, 0
	not	al

	call	printsymandcode

	; print newline at every 16th symbol; otherwise print whitespace
	cmp	ah, 15
	jne	else_whitespace	

if_newline:
	mov	ah, 02h
	mov	dl, 13
	int	21h
	mov	ah, 02h
	mov	dl, 10
	int	21h
	jmp	endif

else_whitespace:
	mov	ah, 03h
	int	10h
	add	dl, 1
	mov	ah, 02h
	int	10h
	jmp	endif
endif:

	pop cx
	loop	loop

	; loop does not include the last character
	mov	al, 255
	call	printsymandcode

	; exit the program
	mov	ah, 4ch
	int	21h


; FUNCTIONS

printsymandcode: ; al = symbol code to print -> al, ah = symbol hex code first and last 4 bits
	mov	bl, al
	call	printsym

	mov	ah, 0
	mov	bl, 16
	div	bl

	mov	bl, '_'
	call	printsym

	mov	bl, al
	call	printhexdigit

	mov	bl, ah
	call	printhexdigit

	ret

printhexdigit: ; bl = digit to print
	push	ax
	push	bx
	push	cx
	
	mov	bh, 0
	mov	bl, digits[bx]
	call	printsym

	pop	cx
	pop	bx
	pop	ax
	ret

printsym: ; bl = symbol code to print
	push	ax
	push	bx
	push	cx
	
	mov	bh, 0
	mov	al, bl
	mov	ah, 0ah
	mov	cx, 1
	int	10h

	mov	ah, 03h
	int	10h
	add	dl, 1
	mov	ah, 02h
	int	10h

	pop	cx
	pop	bx
	pop	ax
	ret


segment stack stack 
        resb 64 
stacktop:
