org 100h

section .data
filename db "MYFILE.BIN",0
msg_nofile db "File MYFILE.BIN is not found. That file shall contain a sequence of keystrokes.",10,13,"$"
keycode db 0
position dd 0
old_1ch dd 0

section .text
	mov	ah, 35h
	mov	al, 1ch
	mov	dx, repeat_handler
	int	21h

	mov	word [old_1ch], bx
	mov	word [old_1ch+2], es

	;hook handler to bios timer tick interrupt
	mov	ah, 25h
	mov	al, 1ch
	mov	dx, repeat_handler
	int	21h

	;terminate and stay resident
	mov	ah, 31h
	mov	al, 0
	mov	dx, 0400h
	int	21h

repeat_handler:
	mov	bx, cs
	mov	ds, bx

	mov	ah, 3dh
	mov	al, 0h
	lea	dx, [filename]
	int	21h
	jnc	open_success

	mov	ah, 09h
	mov	dx, msg_nofile
	int	21h

	;simulate enter press to get on new line :)
	mov	al, 1ch
	call	ps2wr
	
	jmp	unhook
open_success:
	mov	bx, ax

	;move file pointer to saved position
	mov	ah, 42h
	mov	al, 0 ; SEEK_SET
	mov	cx, word [position+2]
	mov	dx, word [position]
	int	21h

	;read one byte from file
	mov	ah, 3fh
	mov	cx, 1
	lea	dx, [keycode]
	int	21h
	jc	unhook

	;check for EOF
	cmp	ax, 0
	je	unhook	

	;simulate a keypress
	mov	al, [keycode]
	call	ps2wr

	;get new pointer position and save it
	mov	ah, 42h
	mov	al, 1 ; SEEK_CUR
	mov	cx, 0
	mov	dx, 0
	int	21h
	mov	[position + 2], dx
	mov	[position], ax

	;close file
	mov	ah, 3eh
	int	21h
	iret
unhook:
	mov	dx, word [old_1ch]
	mov	ds, word [old_1ch+2]

	mov	ah, 25h
	mov	al, 1ch
	int	21h

	iret

;based on https://forum.osdev.org/viewtopic.php?t=24277
;also see http://www-ug.eecg.toronto.edu/msl/nios_devices/datasheets/PS2%20Keyboard%20Protocol.htm

;dl=1 read, dl=2 write
ps2wait:
	push	cx
	mov	cx, 1000
ps2loop:
	in	al, 64h
	and	al, dl
	jnz	ret
	dec	cx
	jnz	ps2loop
ret:
	pop	cx
	ret
ps2wr:
	push	dx
	mov	dh, al
	mov	dl, 2
	call	ps2wait
	mov	al, 0D2h
	out	64h, al
	call	ps2wait
	mov	al, dh
	out	60h, al
	pop	dx
ps2rd:
	push	dx
	mov	dl, 1
	call	ps2wait
	in	al, 60h
	pop	dx
	ret
