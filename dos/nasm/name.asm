org 100h

section .bss
program_name resb 123

section .text 
start:
	push	ds

	mov	bx, [2ch]
	mov	ds, bx
	mov	bx, 0
find:
	add	bx, 1

	cmp	word [bx], 0
	je	found

	jmp	find

found:
	mov	cx, 0
	add	bx, 4
save:
	mov	dl, [bx]
	mov	ax, ds

	pop	ds
	xchg	cx, bx
	mov	byte [program_name + bx], dl
	xchg	cx, bx
	add	cx, 1
	push	ds
	mov	ds, ax

	add	bx, 1
	cmp	byte [bx], 0
	jne	save

	; bring back ds
	pop	ds
	; end string with $
	xchg	bx, cx
	mov	byte [program_name + bx], "$"
	xchg	bx, cx

	; print
	mov	ah, 09h
	lea	dx, [program_name]
	int	21h

	mov	ah, 4ch
	int	21h
