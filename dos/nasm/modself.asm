org 100h

section .data
hello db "Hello!",10,13,"$"
program_name times 123 db 0
buffer times 12 db 0

section .text 
start:
	push	ds

	mov	bx, [2ch]
	mov	ds, bx
	mov	bx, 0
find:
	add	bx, 1

	cmp	word [bx], 0
	je	found

	jmp	find

found:
	mov	cx, 0
	add	bx, 4
print:
	mov	dl, [bx]
	mov	ax, ds

	pop	ds
	xchg	cx, bx
	mov	byte [program_name + bx], dl
	xchg	cx, bx
	add	cx, 1
	push	ds
	mov	ds, ax

	add	bx, 1
	cmp	byte [bx], 0
	jne	print

	pop	ds

	xchg	bx, cx
	mov	byte [program_name + bx], 0
	xchg	bx, cx

	mov	ah, 3dh
	mov	al, 02 ; read and write
	lea	dx, [program_name]
	int	21h

	mov	bx, ax ; handle

	mov	ah, 42h
	mov	al, 02 ; SEEK_END
	mov	dx, 0
	mov	cx, 0
	int	21h

	mov	cx, dx
	mov	dx, ax

	push	cx
	push	dx
	sub	dx, 12

	mov	ah, 42h
	mov	al, 00 ; SEEK_SET
	int	21h

	mov	ah, 3Fh
	mov	cx, 12
	lea	dx, [buffer]
	int	21h

	mov	ah, 3Fh
	mov	cx, 12
	lea	dx, [buffer]
	int	21h

	pop	dx
	pop	cx
	sub	dx, 4

	mov	ah, 42h
	mov	al, 00 ; SEEK_SET
	int	21h

	mov	ah, 40h
	mov	cx, 12
	lea	dx, [buffer]
	int	21h

	jmp	section.tail.start

section tail follows=.data
	mov	ah, 09h
	lea	dx, [hello]
	int	21h

	mov	ah, 4ch
	int	21h
