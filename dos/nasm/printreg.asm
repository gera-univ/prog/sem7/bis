org 100h

section .data
text_sp: db "SP=$"
text_ds: db 10,13,"DS=$"
text_es: db 10,13,"ES=$"
text_ss: db 10,13,"SS=$"
text_cs: db 10,13,"CS=$"

section .text
start:
	lea	dx, [text_sp]
	mov	ah, 09h
	int	21h
	mov	ax, sp
	call	print_word

	lea	dx, [text_ds]
	mov	ah, 09h
	int	21h
	mov	ax, ds
	call	print_word

	lea	dx, [text_es]
	mov	ah, 09h
	int	21h
	mov	ax, es
	call	print_word

	lea	dx, [text_ss]
	mov	ah, 09h
	int	21h
	mov	ax, ss
	call	print_word
	mov	ah, 4ch

	lea	dx, [text_cs]
	mov	ah, 09h
	int	21h
	mov	ax, cs
	call	print_word

	mov	ah, 4ch	
	int	21h

print_word:
	push	ax

	xchg	al, ah
	call	print_byte
	xchg	al, ah
	call	print_byte

	pop	ax
	ret

print_byte:
	push	ax
	push	bx
	mov	ah, 0
	mov	bl, 16
	div	bl

	call	print_4bit
	mov	al, ah
	call	print_4bit

	pop	bx
	pop	ax
	ret

print_4bit:
	push	ax
	push	dx
	cmp	al, 10
	jge	letter	
	mov	dl, '0'
	jmp	end
letter:
	sub	al, 10
	mov	dl, 'A'
end:
	add	dl, al
	mov	ah, 02h
	int	21h
	pop	dx
	pop	ax
	ret
