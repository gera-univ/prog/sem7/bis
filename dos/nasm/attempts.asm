; This program asks for password and if it is incorrect
; it reduces the attempt counter stored as the last byte in 
; program binary (COM file). When the counter reaches 0, the program
; deletes its binary. When the correct password is entered
; the initial attempt count is restored.
org 100h

MAX_ATTEMPTS equ 3

; nasm puts .data section last in the program binary when 'bin' format is used
section .data
password db "hello$"
msg_password db "Password?",10,13,"$"
msg_password_correct db 10,13,"Password is correct. Attempt count is restored.$"
msg_password_incorrect db 10,13,"Password is incorrect. $"
msg_attempt_count_reduced db "Attempt count is reduced.$"
msg_self_destruction db "Self-destruction!$"
program_path times 123 db 0
attempts db MAX_ATTEMPTS ; last byte in program binary

section .text
start:
	call	get_path

	mov	ah, 3dh
	mov	al, 02 ; read and write
	lea	dx, [program_path]
	int	21h

	mov	bx, ax ; handle

	call	read_attempts

	call	ask_password 

	cmp	dl, '$'
	jne	if_incorrect

	mov	byte[attempts], MAX_ATTEMPTS
	call	write_attempts

	mov	ah, 09h
	lea	dx, [msg_password_correct]
	int	21h

	jmp	exit

if_incorrect:
	mov	ah, 09h
	lea	dx, [msg_password_incorrect]
	int	21h

	mov	dl, [attempts]
	sub	dl, 1

	cmp	dl, 0
	jne	attempts_remained	
	call	delete_file
	je	exit

attempts_remained:
	mov	[attempts], dl

	mov	ah, 09h
	lea	dx, [msg_attempt_count_reduced]
	int	21h

	call	write_attempts

exit:
	mov	ah, 4ch
	int	21h

; procedures

; reads path to program binary
; into the program_path variable in the data section
get_path:
	push	ds

	mov	bx, [2ch]
	mov	ds, bx
	mov	bx, 0
find:
	add	bx, 1

	cmp	word [bx], 0
	je	found

	jmp	find

found:
	mov	cx, 0
	add	bx, 4
copy_byte:
	mov	dl, [bx]
	mov	ax, ds

	pop	ds
	xchg	cx, bx
	mov	byte [program_path + bx], dl
	xchg	cx, bx
	add	cx, 1
	push	ds
	mov	ds, ax

	add	bx, 1
	cmp	byte [bx], 0
	jne	copy_byte	

	pop	ds

	xchg	bx, cx
	mov	byte [program_path + bx], 0
	xchg	bx, cx
	ret

; reads attempt count from the last byte in program binary (handle in BX)
; into the attempts variable in data section
read_attempts:
	mov	ah, 42h
	mov	al, 02 ; SEEK_END
	mov	dx, 0
	mov	cx, 0
	int	21h

	mov	cx, dx
	mov	dx, ax
	sub	dx, 1

	mov	ah, 42h
	mov	al, 00 ; SEEK_SET
	int	21h

	mov	ah, 3fh
	mov	cx, 1
	lea	dx, [attempts]
	int	21h
	ret

; writes attempt count from the attempts variable in data section
; into the last byte in program binary (handle in BX)
write_attempts:
	mov	ah, 42h
	mov	al, 02 ; SEEK_END
	mov	dx, 0
	mov	cx, 0
	int	21h

	mov	cx, dx
	mov	dx, ax
	sub	dx, 1

	mov	ah, 42h
	mov	al, 00 ; SEEK_SET
	int	21h

	mov	ah, 40h
	mov	cx, 1
	lea	dx, [attempts]
	int	21h
	ret

; deletes program binary
delete_file:
	mov	ah, 41h
	lea	dx, [program_path]
	int	21h

	mov	ah, 09h
	lea	dx, [msg_self_destruction]
	int	21h

	ret

; checks for password by reading characters and comparing
; to the password in data section until '$' is encountered
; password is correct if '$' is left in DL after the call
ask_password:
	push	bx
	mov	ah, 09h
	lea	dx, [msg_password]
	int	21h

	mov	bx, 0

read_password:
	mov	dl, [password+bx]
	add	bx, 1
	cmp	dl, '$'
	je	ask_password_return

	mov	ah, 01h
	int	21h

	cmp	al, dl
	je	read_password

ask_password_return:
	pop	bx
	ret
