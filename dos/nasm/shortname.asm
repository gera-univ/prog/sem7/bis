; 10. ������� �ணࠬ�� (��� �ணࠬ��� ���������� ᮢ��㯭���� ������ ���
; ������権 ���, ����� ��᫥ �� ������������� �����஬ (��
; �����_���� ������ ����������� ���������) ����� ���⠢���
; ��� ��� �믮����� ��ண� ��।������ ����⢨�) �뢮�� �� ��࠭ ᢮���
; ᮪�饭���� �����.

; nasm shortname.asm -f bin -o shortname.com

; dosbox-x, lfn=true
;   C:\shortname.com
;   SHORTNAM.COM

; dosbox
;   C:\>SHORTN~2.COM
;   SHORTN~2.com

	org 100h

section .bss
program_path	resb 64

section	.text
	mov	bx, ds:[2ch]
	mov	es, bx
	mov	bx, 0
find:
	add	bx, 1

	cmp	word es:[bx], 0
	je	found

	jmp	find

found:
	mov	cx, 0
	mov	ax, 0
	add	bx, 4
copy_byte:
	mov	dl, es:[bx]

	xchg	cx, bx
	mov	byte ds:[program_path + bx], dl
	xchg	cx, bx
	add	cx, 1

	add	bx, 1
	cmp	byte es:[bx], '\'
	jne	no_slash
	mov	ax, cx ; remember position of the last '\'
no_slash:
	cmp	byte es:[bx], 0
	jne	copy_byte

	xchg	bx, cx
	mov	byte ds:[program_path + bx], '$'
	xchg	bx, cx

	add	ax, 1 ; skip '\' itself
	mov	bx, ax

	mov	ah, 09h
	lea	dx, [program_path + bx] ; print program path after the last '\'
	int	21h

	mov	ah, 4ch
	int	21h
