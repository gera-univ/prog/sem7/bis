org 100h

section .data
hello db "hello, wo-ACK!",10,13,"$"

section .bss
program_name resb 123

section .text 
start:
	push	ds

	mov	bx, [2ch]
	mov	ds, bx
	mov	bx, 0
find:
	add	bx, 1

	cmp	word [bx], 0
	je	found

	jmp	find

found:
	mov	cx, 0
	add	bx, 4
print:
	mov	dl, [bx]
	mov	ax, ds

	pop	ds
	xchg	cx, bx
	mov	byte [program_name + bx], dl
	xchg	cx, bx
	add	cx, 1
	push	ds
	mov	ds, ax

	add	bx, 1
	cmp	byte [bx], 0
	jne	print

	pop	ds

	xchg	bx, cx
	mov	byte [program_name + bx], 0
	xchg	bx, cx

	mov	ah, 41h
	lea	dx, [program_name]
	int	21h

	mov	ah, 09h
	lea	dx, [hello]
	int	21h

	mov	ah, 4ch
	int	21h
