org 100h

section .data
hardcoded_path: db "C:\AUTH.COM",0

program_path: times 64 db 0
msg_success: db " $"
msg_fail: db "Go to sleep$"
msg_failpath: db "Hello, redish! Go to Hell, กํฉกจ!$"

password:
db "YHS"
program_size: times 6 db 0

input_buffer:
db 100
db 100
prompt:
times 100 db 0

section .text
	call	get_path

	mov	bx, 0
check_path:
	mov	al, byte [program_path+bx]
	mov	ah, byte [hardcoded_path+bx]
	inc	bx
	cmp	ah, 0
	je	correct_path
	cmp	al, ah
	je	check_path

	mov	ah, 09h
	lea	dx, [msg_failpath]
	int	21h
	jmp	exit

correct_path:
	lea	dx, [program_path]
	mov	ah, 3dh
	mov	al, 0
	int	21h

	mov	bx, ax

	; get file length into dx:ax
	mov	ah, 42h
	mov	al, 2
	mov	cx, 0
	mov	dx, 0
	int	21h

	mov	bx, 0
loopdivide:
	mov	cx, 10
	div	cx

	mov	byte [program_size+bx], dl
	add	byte [program_size+bx], '0'
	inc	bx

	mov	dx, 0

	cmp	ax, 0
	jne	loopdivide

	mov	byte [program_size+bx], '$'
	dec	bx

	mov	cx, 0
loopreverse:
	mov	al, byte [program_size+bx] ; al = str[bx]
	xchg	bx,cx
	mov	dl, byte [program_size+bx] ; dl = str[cx]
	xchg	bx,cx
	mov	byte [program_size+bx], dl ; str[bx] = dl
	xchg	bx,cx
	mov	byte [program_size+bx], al ; str[cx] = al
	xchg	bx,cx
	inc	cx
	dec	bx
	cmp	bx, cx
	jg	loopreverse

	;mov	ah, 09h
	;lea	dx, [password]
	;int	21h
	
	mov	ah, 0ah
	lea	dx, [input_buffer]
	int	21h

	mov	bx, 0
check_password:
	mov	al, byte [prompt+bx]
	mov	ah, byte [password+bx]
	inc	bx
	cmp	ah, '$'
	je	correct_password
	cmp	al, ah
	je	check_password
	
	mov	ah, 09h
	lea	dx, [msg_fail]
	int	21h
	jmp	exit

correct_password:
	mov	ah, 09h
	lea	dx, [msg_success]
	int	21h

exit:
	mov	ah, 4ch
	int	21h

get_path:
	mov	bx, [2ch]
	mov	es, bx
	mov	bx, 0
find:
	add	bx, 1

	cmp	word es:[bx], 0
	je	found

	jmp	find

found:
	mov	cx, 0
	add	bx, 4
copy_byte:
	mov	dl, es:[bx]

	xchg	cx, bx
	mov	byte [program_path + bx], dl
	xchg	cx, bx
	add	cx, 1

	add	bx, 1
	cmp	byte es:[bx], 0
	jne	copy_byte	

	xchg	bx, cx
	mov	byte [program_path + bx], 0
	xchg	bx, cx
	ret
