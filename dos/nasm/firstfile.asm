	org 100h

section .text
	mov	ah, 1ah
	mov	dx, dta
	int	21h

	; find first matching file
	mov	ah, 4eh
	mov	cx, 00000000b
	mov	dx, filemask
	int	21h

	jc	error

	; terminate string with '$'
	mov	byte [filename + 13], "$"

	; print filename
	mov	ah, 09h
	mov	dx, filename
	int	21h

exit:
	mov	ah, 4ch
	int	21h

error:
	mov	ah, 09h
	mov	dx, errormsg
	int	21h
	jmp	exit


section .data
	filemask	db "*.*",0
	errormsg	db "error",10,13,"$"
	dta		times 128 db 0
	filename	equ dta + 1eh
